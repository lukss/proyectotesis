using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bullet : MonoBehaviour
{

    public Rigidbody Rb;
    public float speed = 5f;

    void Update()
    {
        Rb.AddForce(gameObject.transform.right *speed, ForceMode.Impulse);
        
     
        Destroy(gameObject, 2f);

    }
}
