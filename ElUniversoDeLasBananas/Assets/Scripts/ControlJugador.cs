using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UIElements;

public class ControlJugador : MonoBehaviour
{
    public static float rapidezDesplazamiento = 7f;
    public Rigidbody rb;
    public float rotationV = 180f;
    public float salto;
    private bool Piso = true;
    public int maxSaltos = 2;
    public int saltoActual = 0;
    public int velRot = 3;
    [SerializeField] float time = 1f;
    private float x, y;
    [SerializeField] private int damage=3;
    bool timer = false;
    Vector3 original;
   public  GameObject UP;
    //  public GameObject Golpe;
    public bool matar=false;
    public Animator animator;
    public GameObject portal;
    private int cont;
    [SerializeField]private int collisionCount = 0;
    public int collisionThreshold = 3;
    Golpe punch;
    public int ContadorJaulas = 0;
    public bool JaulasRecolectadas = false;
    public float fuerzaEmpuje;
    public TMP_Text vida;
    public TMP_Text Items;
    public TMP_Text JaulasRec;
    private Vector3 movimiento;
    public GameObject portaltut;
    public AudioSource audioSource;
   [SerializeField]private AudioClip colectar;
    [SerializeField] private AudioClip colectar2;
    [SerializeField] private AudioClip colectar3;
    void Start()
    {
          rapidezDesplazamiento = 7f;
    rb = GetComponentInChildren<Rigidbody>();

    }

    void Item()
    {

        if (cont >= 4)
        {

            portal.gameObject.SetActive(true);

        }
        if (cont >= 1)
        {
            portaltut.gameObject.SetActive(true);   
        }
    }


    void Update()
    {
        vida.text = damage.ToString();
        Items.text = cont.ToString();
        JaulasRec.text = ContadorJaulas.ToString();
  
        //Golpear();
        //  movAdelanteAtras();
        //movCostados();
          x = Input.GetAxis("Horizontal");
          y = Input.GetAxis("Vertical");



        movimiento = new Vector3(x, 0f, y).normalized;
     
        transform.Translate(movimiento * Time.deltaTime * rapidezDesplazamiento);
        //transform.Translate(x * Time.deltaTime * rapidezDesplazamiento, 0f, 0f);

 
       // transform.Translate(0f, 0f, y * Time.deltaTime * rapidezDesplazamiento);

         //transform.Rotate(0, x * Time.deltaTime * rotationV, 0f);
        // transform.Translate(0, 0, y * Time.deltaTime * rapidezDesplazamiento);

        animator.SetFloat("X", x);
        animator.SetFloat("Y", y);
        if (x > 0||x<0|| y > 0 || y < 0)
        {
            animator.SetBool("Other", true);
        }
        if (Input.GetButtonDown("Jump") && (Piso || maxSaltos > saltoActual))
        {
           // animator.SetBool("Other", true);
            animator.Play("Jump");
            // Golpe.gameObject.SetActive(false);
            rb.velocity = new Vector3(0f, salto, 0f * Time.deltaTime);
            rb.AddForce(Vector3.up * salto, ForceMode.Impulse);
            Piso = false;
            saltoActual++;
        }
        if (Input.GetKeyDown(KeyCode.R))
        {
           // Golpe.gameObject.SetActive(false);
            SceneManager.LoadScene(SceneManager.GetActiveScene().name);

        }
        //  animator.SetBool("Other", false);
        //animator.SetFloat("Y", rapidezDesplazamiento);
        if (time > 0f&& timer==true&& matar==true)
        {
            time-=Time.deltaTime;
          
        }
        else if (time <= 0f )
        {
            // transform.localScale = original;
            rapidezDesplazamiento=7f;
            timer = false;
            matar = false;
        }

     
        if (JaulasRecolectadas == true)
        {
            ContadorJaulas+=1;
           JaulasRecolectadas = false;
        }

        if (Input.GetKeyDown(KeyCode.C))
        {
            
            animator.Play("Agachar");

        }

        
    }



    public void power()
    {
;   //  original = transform.localScale;
    // transform.localScale *= 3f;
        rapidezDesplazamiento=14f;
       UP.gameObject.SetActive(false);
      
        
    }
        
    

    private void OnCollisionEnter(Collision collision)
    {
        Piso = true;
        saltoActual = 0;

       if (collision.gameObject.CompareTag("Enemigo")==true)
        {

            Vector3 direccionAtras = transform.position - collision.gameObject.transform.position;
            direccionAtras.y = 0f;
                 

            direccionAtras = direccionAtras.normalized;


            rb.AddForce(direccionAtras * fuerzaEmpuje*1, ForceMode.Impulse);
            audioSource.PlayOneShot(colectar2);
            damage--;
         
        


    }
        if (damage <= 0)
        {
         
            SceneManager.LoadScene (SceneManager.GetActiveScene().name);   
        }

        if (collision.gameObject.CompareTag("Jaula") == true )
        {
            JaulasRecolectadas = true;
            animator.SetBool("Other", false);
            animator.Play("Abrir");

           
            collision.gameObject.gameObject.SetActive(false);
         
        }
       
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Item") == true)
        {
            audioSource.PlayOneShot(colectar);
            cont = cont + 1;
            other.gameObject.SetActive(false);
            Item();
            
        }

        if (other.gameObject.CompareTag("Power") == true){

           
            power();
            audioSource.PlayOneShot(colectar3);
            matar = true;
             timer = true;

        }
        if (other.gameObject.CompareTag("Bala") == true)
        {
            SceneManager.LoadScene("SampleScene");

        }
       
            if (other.CompareTag("Pinchos") == true)
            {
            audioSource.PlayOneShot(colectar2);
            animator.SetBool("Other", false);
            animator.Play("Lastimado");
                damage--;
            }
        if (damage <= 0)
        {

            SceneManager.LoadScene(SceneManager.GetActiveScene().name);
        }

    }

    
}

