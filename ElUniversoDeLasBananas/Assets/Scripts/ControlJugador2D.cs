using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;

public class ControlJugador2D : MonoBehaviour
{
    private Rigidbody2D rb;
    // Start is called before the first frame update

   
    private float movimientoHorizontal = 0f;
    [SerializeField] private float velocidadDeMovimiento;
    [SerializeField] private float suavizadoDeMovimiento;
    private Vector2 velocidad = Vector2.zero;
    // private bool mirandoAlaDerecha = true;
    private SpriteRenderer spriteRenderer;
    public float fuerzasalto = 6f;
      private bool Piso = true;
    [SerializeField] private int maxSaltos = 2;
    [SerializeField] private int saltos = 0;
    [Header("Animacion")]
    private Animator anim;
    public int cont;
    public GameObject portal2D;
    public int ContadorJaulas = 0;
    public bool JaulasRecolectadas = false;
    public GameObject Enemigo;
    public TMP_Text vida;
    public TMP_Text contJaulas;
    public TMP_Text ItemsCont;
    public int damage=3;
    public GameObject portalTUT2D;
    public AudioSource audioSource;
    [SerializeField] private AudioClip colectar;
    [SerializeField] private AudioClip colectar2;
    void Start()
    {
        rb = GetComponent<Rigidbody2D>();
        saltos = maxSaltos;
        anim = GetComponent<Animator>();
    }

   
    private void Update()
    {
        vida.text=damage.ToString();    
        contJaulas.text=ContadorJaulas.ToString();
        ItemsCont.text = cont.ToString();
        // Pegar();
        movimientoHorizontal = Input.GetAxisRaw("Horizontal") * velocidadDeMovimiento;
        if (movimientoHorizontal > 0 || movimientoHorizontal < 0)
        {
            anim.SetBool("Other", true);
        }
        // anim.SetBool("Enpiso", Piso);
        anim.SetFloat("Horizontal", Mathf.Abs(movimientoHorizontal));
        if (Input.GetKeyDown(KeyCode.Space) && saltos > 0)
        {
            anim.SetBool("Other", false);
            anim.Play("Salto");
            // anim.SetTrigger("Salto");
            Saltar();
        }

        Item();

        if (JaulasRecolectadas == true)
        {
            ContadorJaulas += 1;
            JaulasRecolectadas = false;
        }
        if (damage < 0 || damage==0)
        {
            RestartLevel();
        }
        
    }
    private void RestartLevel()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().name);

    }
    void restart()
    {
        if (Input.GetKeyDown(KeyCode.R)) {

            SceneManager.LoadScene(SceneManager.GetActiveScene().name);

        }
   
    }
    public void Awake()
    {
        spriteRenderer = GetComponent<SpriteRenderer>();
    }
    private void FixedUpdate()
    {
        Mover(movimientoHorizontal*Time.deltaTime);
    }

    void Mover(float mover)
    {
        Vector2 velocidadObjetivo = new Vector2(mover, rb.velocity.y);
        rb.velocity = Vector2.SmoothDamp(rb.velocity, velocidadObjetivo, ref velocidad, suavizadoDeMovimiento);

        /*  if (mover > 0 && !mirandoAlaDerecha)
          {
             // Mano.gameObject.SetActive(false);
              Girar();
          }
          else if(mover <0 && mirandoAlaDerecha)
          {
             // Mano.gameObject.SetActive(false);
              Girar();

          }*/
        if (movimientoHorizontal > 0)
        {
            spriteRenderer.flipX = false;
            anim.SetBool("Other", true);
        }
        
        else if (movimientoHorizontal < 0)
        {
            spriteRenderer.flipX = true;
            anim.SetBool("Other", true);
        }

    }

    void Girar()
    {
       // mirandoAlaDerecha = !mirandoAlaDerecha;
        Vector2 escala = transform.localScale;
        escala.x = 1;
        transform.localScale = escala;
       
    }

    private void Saltar()
    {
        rb.velocity=new Vector2(rb.velocity.x,0f);
        rb.AddForce(new Vector2(0f, fuerzasalto), ForceMode2D.Impulse);
        Piso= false;
        saltos--;
    }
    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.CompareTag("Piso"))
        {
           Piso= true;
            saltos = maxSaltos;
        }
        if (collision.gameObject.CompareTag("Jaula") == true)
        {
            JaulasRecolectadas = true;
          


            collision.gameObject.gameObject.SetActive(false);

        }
      /*  if (collision.gameObject.CompareTag("Eliminador") == true)
        {
            Enemigo.gameObject.SetActive(false);
        }*/ 
    }
    void Item()
    {

        if (cont >= 4)
        {

            portal2D.gameObject.SetActive(true);

        }
        if (cont >= 1)
        {
            portalTUT2D.gameObject.SetActive(true);
        }
    }
    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject.CompareTag("Item") == true)
        {
            audioSource.PlayOneShot(colectar);
            cont = cont + 1;
           // Item();
            other.gameObject.SetActive(false);
        }
      
            if (other.gameObject.CompareTag("Pinchos") == true)
            {
            // SceneManager.LoadScene("N1P2");
            audioSource.PlayOneShot(colectar2);
            anim.SetBool("Other", false);
                anim.Play("hurt");
                damage -= 1;
                


            }
        
        
        
    }
    
}
