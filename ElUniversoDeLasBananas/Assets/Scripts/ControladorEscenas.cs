using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class ControladorEscenas : MonoBehaviour
{

    [SerializeField] private Image color_01;
    [SerializeField] private string NV2P;
    [SerializeField] private float cortinaVel;
    [SerializeField] private bool transicion;
    private float radiusColor_01;
    private bool enter;
    ControlJugador control;
    public GameObject Transporte;

    void Start()
    {
        Inicializar();
    }

    private void Inicializar()
    {
       
        if (!transicion)
        {
            if (color_01 != null)
            {
                radiusColor_01 = 0;
                color_01.material.SetFloat("_Cutoff", radiusColor_01);

            }
        }
        else
        {
            if (color_01 != null)
            {
                radiusColor_01 = 1;
                color_01.material.SetFloat("_Cutoff", radiusColor_01);
            }
        }
        
    }
    public void IniciarTransicion()
    {
        StartCoroutine(ActiveCanvas());
    }
    // Update is called once per frame
    private void Update()
    {
        
        if (enter && color_01.material.GetFloat("_Cutoff") <= 1)
        {
            radiusColor_01 += Time.deltaTime * cortinaVel;
            color_01.material.SetFloat("_Cutoff", radiusColor_01);
        }

            if (transicion && color_01.material.GetFloat("_Cutoff") >= 0)
        {
            radiusColor_01 -= Time.deltaTime * cortinaVel;
            color_01.material.SetFloat("_Cutoff", radiusColor_01);

            if (color_01.material.GetFloat("_Cutoff") <= 0)
            {
                color_01.material.SetFloat("_CutOff", 0);
                transicion = false;

            }
        }


       

    }
   

    IEnumerator ActiveCanvas()
    {
        enter = true;
        yield return new WaitForSeconds(2);
        SceneManager.LoadScene(NV2P);
    }
}
