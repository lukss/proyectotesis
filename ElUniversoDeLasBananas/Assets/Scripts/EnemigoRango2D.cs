using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class EnemigoRango2D : MonoBehaviour
{

    public float rangoDeAlerta; 
    public Transform jugador;
    public float vel;
    public GameObject Banana;
    public float Range = 5f; 
    public float moveSpeed = 3f; 
    private Vector3 initialPosition; 
    private bool Persiguiendo = false; 
    private Rigidbody2D rb; 

    private void Start()
    {
        initialPosition = transform.position;
        rb = GetComponent<Rigidbody2D>();
    }
    /* private void OnDrawGizmosSelected()
     {
         Gizmos.color = Color.red;
         Gizmos.DrawWireSphere(transform.position, rangoDeAlerta);
     }*/
    private void Update()
    {
        /*float distanceToPlayer = Vector2.Distance(transform.position, jugador.position);

        if (distanceToPlayer <= rangoDeAlerta)
        {
            // Mueve el enemigo hacia el jugador
            transform.position = Vector2.MoveTowards(transform.position, jugador.position, vel * Time.deltaTime);
        }*/
     
        float distanceToPlayer = Vector3.Distance(transform.position, jugador.position);

     
        if (distanceToPlayer <= Range)
        {
            Persiguiendo = true;
        }
        else if (Persiguiendo && distanceToPlayer > Range)
        {
           
            Persiguiendo = false;
        }

       
        if (Persiguiendo)
        {
            Vector3 direction = (jugador.position - transform.position).normalized;
            rb.velocity = direction * moveSpeed;
        }
        else
        {
          
            Vector3 direction = (initialPosition - transform.position).normalized;
            rb.velocity = direction * moveSpeed;
        }
    }
    private void OnCollisionEnter2D(Collision2D collision)
    {
       if (collision.gameObject.CompareTag("Player") == true)
        {
            SceneManager.LoadScene("N1P2");    
        }
        
    }
}
