using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour
{
    public int contador = 0;
    public GameObject[] enemigos;



  
   
    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.CompareTag("Player"))
        {
            contador++;
           
            if (contador >= 3)
            {
                foreach (GameObject enemigo in enemigos)
                {
                    Destroy(enemigo);
                }
            }
        }
    }
  
    
}
