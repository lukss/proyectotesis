
using System.Collections; 
using System.Collections.Generic; 
using UnityEngine;
using UnityEngine.SocialPlatforms;
using UnityEngine.UIElements;

public class Golpe : MonoBehaviour
{
    public float fuerzaGolpe = 10f;
    public float duracionGolpe = 0.1f; 
    public float duracionRecuperacion = 1f;

    public bool enGolpe = false;
    private float tiempoGolpe = 0f;
    private float tiempoRecuperacion = 0f;
    public Animator anim;
    [SerializeField] private float ColliderCount = 0;
    [SerializeField] private float maxCollision = 3;
    public GameObject Enem;
    public ControlJugador control;
    public AudioSource audioSource;
    [SerializeField] private AudioClip colectar;
    private void Update()
    {
        if (!enGolpe && tiempoRecuperacion <= 0 && Input.GetKeyDown(KeyCode.F))
        {
            anim.SetBool("Other", false);
            anim.Play("Golpe");
            DetectarGolpe();
            
        }

        if (enGolpe)
        {
            tiempoGolpe += Time.deltaTime;

            if (tiempoGolpe > duracionGolpe)
            {
                enGolpe = false;
                tiempoGolpe = 0f;
                tiempoRecuperacion = duracionRecuperacion;
            }
        }
        else if (tiempoRecuperacion > 0)
        {
            tiempoRecuperacion -= Time.deltaTime;
        }
    }

    private void DetectarGolpe()
    {
        enGolpe = true;
        audioSource.PlayOneShot(colectar);
        Collider[] cols = Physics.OverlapSphere(transform.position, 3.8f);
        
        foreach (Collider col in cols)
        {
            if (col.gameObject != gameObject && col.gameObject.CompareTag("Enemigo"))
            {
                ColliderCount++;
                Rigidbody rb = col.gameObject.GetComponent<Rigidbody>();

                if (rb != null)
                {
                    Vector3 direccion = (col.transform.position - transform.position).normalized;
                    rb.AddForce(direccion * fuerzaGolpe, ForceMode.Impulse);

                }
                if (ColliderCount >= maxCollision)
                {
        
                    col.gameObject.SetActive(false); 
                    ColliderCount = 0f;
                    
                }
                if (col.gameObject != gameObject && col.gameObject.CompareTag("Enemigo") && control.matar==true)
                
                {
                    col.gameObject.SetActive(false);
                }
            

            }
            
        }

        
    }

    

}

