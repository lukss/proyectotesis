using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.U2D;

public class Golpe2D : MonoBehaviour
{
    public float fuerzaGolpe = 10f;
    public float duracionGolpe = 0.1f;
    public float tiempoRecuperacion = 1f;
    public Animator anim;
   
    private bool enGolpe = false; 
    private float tiempoGolpe = 0f;
    private float tiempoRestanteRecuperacion = 0f;

    private void Update()
    {
        
        if (!enGolpe && tiempoRestanteRecuperacion <= 0 && Input.GetKeyDown(KeyCode.F))
        {
            anim.SetBool("Other", false);
            anim.Play("Golpe");
            EmpezarGolpe();
        }

       
        if (enGolpe)
        {
            tiempoGolpe += Time.deltaTime;

            if (tiempoGolpe > duracionGolpe)
            {
                TerminarGolpe();
            }
        }
        else if (tiempoRestanteRecuperacion > 0)
        {
            
            tiempoRestanteRecuperacion -= Time.deltaTime;
        }
    }

    private void EmpezarGolpe()
    {
       
        enGolpe = true;

    
        Collider2D[] objetosCercanos = Physics2D.OverlapCircleAll(transform.position, 2.5f);

      
        foreach (Collider2D objetoCercano in objetosCercanos)
        {
            if (objetoCercano.gameObject != gameObject && objetoCercano.gameObject.CompareTag("Enemigo"))
            {
                Rigidbody2D rigidbodyObjetoCercano = objetoCercano.gameObject.GetComponent<Rigidbody2D>();

                if (rigidbodyObjetoCercano != null)
                {
                    Vector2 direccionGolpe = (objetoCercano.transform.position - transform.position).normalized;
                    rigidbodyObjetoCercano.AddForce(direccionGolpe * fuerzaGolpe, ForceMode2D.Impulse);
                }
            }
        }
    }

    private void TerminarGolpe()
    {
       
        enGolpe = false;      
        tiempoGolpe = 0f;       
       tiempoRestanteRecuperacion = tiempoRecuperacion;
    }
}