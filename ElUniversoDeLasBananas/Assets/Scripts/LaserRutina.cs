using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LaserRutina : MonoBehaviour
{
        public Transform punto1;
        public Transform punto2;
        public float velocidad = 3f;
        private bool traslado = true;
        private float T = 0f;

        void Start()
        {

        }

        private void Update()
        {
            T += Time.deltaTime * velocidad;

            if (traslado)
            {
                transform.position = Vector3.Lerp(punto2.position, punto1.position, T);


            }
            else
            {
                transform.position = Vector3.Lerp(punto1.position, punto2.position, T);
            }

            if (T >= 1f)
            {
                traslado = !traslado;
                T = 0f;
            }
        }
    }
