using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MenuManager : MonoBehaviour
{
    public void Jugar()
    {
        SceneManager.LoadScene("N1");
    }
    public void Tutorial()
    {
        SceneManager.LoadScene("Tutorial");
    }
    public void Salir()
    {
        Application.Quit(); 
    }

    public void Parte23D()
    {
        SceneManager.LoadScene("N2P3D");
    }
    public void Parte32D()
    {
        SceneManager.LoadScene("N3P2D");
    }
    public void Menu()
    {
        SceneManager.LoadScene("Menu");
    }
    public void JugarTut()
    {
        SceneManager.LoadScene("CinematicaInicial");
    }
    public void credits()
    {
        SceneManager.LoadScene("EscenaFinal");
    }
}
