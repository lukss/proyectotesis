using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShotEnem2 : MonoBehaviour
{
    [SerializeField]
    GameObject Hand;

    [SerializeField]
    GameObject Bullet;

    float Counter;

    int SpawnTime = 5;

    void Start()
    {

    }


    void Update()
    {
        Hand.gameObject.SetActive(false);
        Counter += Time.deltaTime;

        if (Counter > SpawnTime)
        {

            Hand.gameObject.SetActive(true);
            Instantiate(Bullet, Hand.transform.position, Hand.transform.rotation);
            Counter = 0;
        }

    }



}

  
