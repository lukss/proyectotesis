using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Transporte : MonoBehaviour
{
    private ControladorEscenas controladorEscenas;

    private void Start()
    {
        controladorEscenas = FindObjectOfType<ControladorEscenas>();
    }

    
    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Player"))
        {
            controladorEscenas.IniciarTransicion();
        }
    }
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.CompareTag("Player"))
        {
            controladorEscenas.IniciarTransicion();
        }
    }
}
